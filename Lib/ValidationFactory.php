<?php

namespace dev108\Validation;

use Illuminate\Validation;
use Illuminate\Translation;
use Illuminate\Filesystem\Filesystem;

class ValidationFactory
{
    private $factory;

    public function __construct(string $lang = 'mn')
    {
        $this->factory = new Validation\Factory(
            $this->loadTranslator($lang)
        );
    }

    protected function loadTranslator(string $lang = '')
    {
        $filesystem = new Filesystem();
        $loader = new Translation\FileLoader(
            $filesystem, dirname(dirname(__FILE__)).'/lang');
        $loader->addNamespace(
            'lang',
            dirname(dirname(__FILE__)).'/lang'
        );

        if ($lang === '') {
            $lang = mb_strtolower(LANG) ?? 'en';
        }

        $loader->load($lang, 'validation', 'lang');
        return new Translation\Translator($loader, mb_strtolower($lang));
    }

    public function __call($method, $args)
    {
        return call_user_func_array(
            [$this->factory, $method],
            $args
        );
    }

    public static function create(array $data, array $rules, array $messages = [], array $customAttributes = [])
    {
        $validator = new ValidationFactory();
        return $validator->make($data, $rules, $messages, $customAttributes);
    }
}