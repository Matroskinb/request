<?php

namespace dev108\Validation\Service;


interface ValidatableInterface
{

  public function with(array $input);

  public function passes();

  public function errors();

}