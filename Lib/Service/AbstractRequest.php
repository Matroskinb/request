<?php

namespace dev108\Validation\Service;


use dev108\Validation\ValidationFactory;
use Illuminate\Validation\DatabasePresenceVerifier;
use Illuminate\Validation\ValidationException;
use Illuminate\Validation\Validator;
use led\Helpers\Notify;
use led\Log\Log;
use led\System\Input;

class AbstractRequest extends AbstractValidator implements ValidatableInterface
{

    protected $authorizeResult = [];

    public function authorizeResult()
    {
        return $this->authorizeResult;
    }

    protected function onValidationFails(Validator $validator)
    {
        Log::notice("validation error data", ['post' => $_POST, 'get' => $_GET, 'errors' => $this->errors]);
        Notify::add('VALIDATION_ERROR', '[t]Ошибка[/t]', 'danger', $validator->messages()->toArray());
        http_response_code(422);

        throw new ValidationException($validator);
    }

    public function validateAllInput()
    {
        return $this->with(array_merge(
                Input::allPost(),
                Input::allFiles(),
                json_decode(file_get_contents('php://input'), true))
        )->passesWithData();
    }
}