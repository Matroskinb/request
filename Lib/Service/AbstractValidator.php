<?php

namespace dev108\Validation\Service;


use dev108\Validation\ValidationFactory;
use Illuminate\Validation\DatabasePresenceVerifier;
use Illuminate\Validation\ValidationException;
use Illuminate\Validation\Validator;
use led\Log\Log;
use led\System\Input;

class AbstractValidator implements ValidatableInterface
{

    protected $validator;

    protected $data = [];

    protected $errors = [];

    protected $rules = [];

    protected $messages = [];


    public function __construct($connection = null, $lang = 'mn')
    {
        $this->validator = new ValidationFactory($lang);

        if ($connection) {

            $dbPresenceVerifier = new DatabasePresenceVerifier($connection);

            $this->validator->setPresenceVerifier($dbPresenceVerifier);
        }
    }

    public function with($data = [])
    {
        $this->data = $data;

        return $this;
    }

    public function getRules()
    {
        return $this->rules;
    }

    public function make(): Validator
    {

        return $this->validator
            ->make(
                $this->data ?? [],
                $this->getRules(),
                $this->messages
            )
            ->after(function (Validator $validator): Validator {
                return $this->afterValidation($validator);
            });
    }

    public function handleFailValidation($validator)
    {
        $this->errors = $validator->messages()->all();
        $this->onValidationFails($validator);
    }

    public function afterValidation(Validator $validator): Validator
    {
        if (method_exists($this, 'authorize')) {
            // authorization function exists
            $this->authorizeResult = $this->authorize();
            if (!$this->authorizeResult) {
                $validator->errors()->add('access', '[t]Access denied[/t]');
            }
        }

        return $validator;
    }

    public function passes()
    {
        $validator = $this
            ->make();

        if ($validator->fails()) {
            $this->handleFailValidation($validator);
            return false;
        }

        return $validator;
    }

    /**
     * @return array|bool
     * @throws \Illuminate\Validation\ValidationException
     */
    public function passesWithData()
    {
        $validator = $this->make();
        try {
            $data = $validator->validate();

            return $data;
        } catch (\Illuminate\Validation\ValidationException $e) {
            $this->handleFailValidation($validator);
            return false;
        }
    }

    public function errors()
    {
        return $this->errors;
    }

    protected function onValidationFails(Validator $validator)
    {
        return false;
    }
}